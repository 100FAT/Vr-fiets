// voorbeeld programma voor VR fiets v2.0
// de rem functie sturing op een arduino mega pin 2
// door Peter de Man 2018

void setup(void){
  pinMode(A0, INPUT);   // test potmeter om de mee te kunnen instellen.
  pinMode(2, OUTPUT);   // pwm op pin 2
  TCCR3B = TCCR3B & 0b11111000 | 1;       // custom PWM setting, deze is een must !!!!!!!!!!!!!!! anders maakt de motor een raar geluid en werk het niet lekker !!!
  return;
}

unsigned int giPotmeter;
unsigned char giPwmDutyCycle;             // range = 0-255     

void loop(void){
  giPotmeter = analogRead(A0);            // range 0-1023
  giPwmDutyCycle = giPotmeter/4;          // convert range
  analogWrite(2, giPwmDutyCycle );        // range = 0-255      // 255 is maximaal remmen
  return;  
}

