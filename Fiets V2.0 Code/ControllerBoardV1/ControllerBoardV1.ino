#include <Encoder.h>
// Controller script to control the speed of the vr-bike motor

/*****[Motor PWM]*****/
const int motorPin = 2;
int currentMotorValue;
int targetMotorValue;

unsigned long previousTimeFade = 0;
unsigned long previousTimeMotor = 0;
unsigned long currentTime = 0;
unsigned long timeOutMotor = 120000; //2 minutes

/*****[Steering wheel]*****/
Encoder wheelEncoder(18,19);
const int wheelIndexPin = 20;
const int ZEROPOINT = -775;
long wheelPosition = 0;

/*****[Reed sensor]*****/
const int reedPin = 3;
volatile int reedCounter = 0;
int distanceTraveled = 0;
const int distancePerPulse = 8; //cm

unsigned long previousTimeReed = 0;
const unsigned long intervalTimeReed = 20;



void setup(){
	
	pinMode(motorPin, OUTPUT);
	//Setting the pwm frequency higher so the motor will work properly(DO NOT OMIT THIS!)
	TCCR3B = TCCR3B & 0b11111000 | 1;

	//Setting interrupt pins
	pinMode(wheelIndexPin, INPUT);
	attachInterrupt(digitalPinToInterrupt(wheelIndexPin), wheelIndexInterrupt, FALLING);

	pinMode(reedPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(reedPin), reedInterrupt, FALLING);
	Serial.begin(115200);
	//for debugging 
	Serial.setTimeout(10);
	//Serial.println("Connection made");
}

void wheelIndexInterrupt(){

	wheelEncoder.write(ZEROPOINT);
	//debug line
	//Serial.println("****ZEROPOINT SET*****");
}

void reedInterrupt(){
	reedCounter ++;
}

void loop(){
	//get a timestamp
	currentTime = millis();

	//if something has been send over Serial
	if(Serial.available() > 0){
		//record the timeStamp
		previousTimeMotor = millis();
		//get the value from the serialPort
		//targetMotorValue = Serial.read();

		//for debuggin
		targetMotorValue = Serial.parseInt();
	}

	//if the targetMotorValue has not been changed for x amount of minutes
	if((unsigned long)(currentTime - previousTimeMotor) >= timeOutMotor){
		//we change the target to 0 to prevent the controller board burning out
		targetMotorValue = 0;
	}

	fadeMotor(targetMotorValue);

	wheelPosition = wheelEncoder.read();

	//check if the interval time to send stuff to unit has passed
	if((unsigned long)(currentTime - previousTimeReed) >= intervalTimeReed){
		//get the amount of pulses from the reedsensor and multiply by the distanceperpulse
		distanceTraveled = distancePerPulse * reedCounter;
		//reset the reedCounter for the next time
		reedCounter = 0;
		//update the timer
		previousTimeReed = currentTime;

		//send the data to unity
		Serial.print("+");
		Serial.print(distanceTraveled);
		Serial.print(",");
		Serial.println(wheelPosition);
	}	
}

void fadeMotor( int targetValue ){

	//delaytime time when you fade from 0 to 255 in 2040 ms
	unsigned long currentTime = millis();

	long delayTime = 2040/255;

	if((unsigned long)(currentTime - previousTimeFade) >= delayTime){
		//if the last time we did this was longer ago then when the delayTime
		if(targetValue != currentMotorValue){
			
			if 		(currentMotorValue < targetValue){
				//increments the current value to match the targetValue 
				currentMotorValue +=1;	
			}

			else if (currentMotorValue > targetValue){
				//decrement the current value to match the targetValue 
				currentMotorValue -=1;
			}
		}
		previousTimeFade = currentTime;
	}
	//send the faded pwmtime to the motor
	analogWrite(motorPin, currentMotorValue);
}