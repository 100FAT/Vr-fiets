#include <Encoder.h>
// Controller script to control the speed of the vr-bike motor

/*****[Motor PWM]*****/
const int motorPin = 2;
int currentMotorValue;
bool targetMotorValue;

unsigned long previousTimeFade = 0;
unsigned long previousTimeMotor = 0;
unsigned long currentTime = 0;
unsigned long timeOutMotor = 300000; //2 minutes

/*****[Steering wheel]*****/
Encoder wheelEncoder(18,19);
const int wheelIndexPin = 20;
const int ZEROPOINT =775;
long wheelPosition = 0;

/*****[Reed sensor]*****/
const int reedPin = 3;
volatile int reedCounter = 0;
int distanceTraveled = 0;
const int distancePerPulse = 99; //cm

unsigned long previousTimeReed = 0;

const unsigned long intervalTimeReed = 20;



void setup(){
	
	//Setting interrupt pins
	pinMode(wheelIndexPin, INPUT);
	attachInterrupt(digitalPinToInterrupt(wheelIndexPin), wheelIndexInterrupt, FALLING);

	pinMode(reedPin, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(reedPin), reedInterrupt, FALLING);
	Serial.begin(115200);
}

void wheelIndexInterrupt(){
	//When the index pin has been triggered by the encoder, reset the position
	wheelEncoder.write(ZEROPOINT);
}

void reedInterrupt(){
	//Record the fact the magnet has passed the reed sensor
	reedCounter ++;
}

void loop(){
	//get a timestamp
	currentTime = millis();

	//if something has been send over Serial
	if(Serial.available() > 0){
		//record the timeStamp
		previousTimeMotor = millis();
		//get the value from the serialPort
		targetMotorValue = Serial.read();
	}

	//if the targetMotorValue has not been changed for x amount of minutes
	if((unsigned long)(currentTime - previousTimeMotor) >= timeOutMotor){
		//we change the target to 0 to prevent the controller board burning out
		targetMotorValue = 0;
	}

	//Set the motor on or off
	digitalWrite(motorPin, targetMotorValue);

	wheelPosition = wheelEncoder.read();

	//check if the interval time to send stuff to unity has passed
	if((unsigned long)(currentTime - previousTimeReed) >= intervalTimeReed){
		//get the amount of pulses from the reedsensor and multiply by the distanceperpulse
		distanceTraveled = distancePerPulse * reedCounter;
		//reset the reedCounter for the next time
		reedCounter = 0;
		//update the timer
		previousTimeReed = currentTime;

		//send the data to unity
		Serial.print("+");
		Serial.print(distanceTraveled);
		Serial.print(",");
		Serial.println(wheelPosition);
	}	
}