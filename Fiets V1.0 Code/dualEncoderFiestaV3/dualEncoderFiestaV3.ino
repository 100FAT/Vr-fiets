#include <Encoder.h>
#include <SoftwareSerial.h>

//Variables STEER
Encoder steerEncoder(18,19);
static int STEERINDEXPIN = 2;

static int ZEROPOINT = -775;
long steerPosition = 0;

//Variables Distance
static int DISTANCEINDEXPIN = 3;
volatile int indexCounterDistance = 0;
int distanceTraveled = 0;

//Time Valiables
unsigned long previousTime = 0;
unsigned long currentTime = 0;
unsigned long timeDifference = 0;
static unsigned long INTERVALTIME = 20;

//Variables for communicating with peters board
byte motorValue = 0;	
byte checksum  = 0;

void setup() {

	Serial.begin(115200);
	Serial.println("Connection made");

	//Serial connection for motorboard using TX 16 nd RX17
	Serial2.begin(19200);

	pinMode(DISTANCEINDEXPIN , INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(DISTANCEINDEXPIN), distanceIndexInterrupt, FALLING);

	pinMode(STEERINDEXPIN, INPUT);
	attachInterrupt(digitalPinToInterrupt(STEERINDEXPIN), steerIndexInterrupt, FALLING);
}

void distanceIndexInterrupt(){
	indexCounterDistance ++;
}

void steerIndexInterrupt(){
	//When the indexpin in the encoder triggers, recalibrate the encoder e.g set the position to ZEROPOINT
	Serial.println("TIGGER!");
	steerEncoder.write(ZEROPOINT);
}

void loop() {
	//calculate how much time has passed
	currentTime = millis();
	timeDifference = currentTime - previousTime;
	steerPosition = steerEncoder.read();

	if(Serial.available() > 0) {

		motorValue = Serial.read();
		
		//Comprise the data to send to the motorboard
		//Calculate the checksum
		checksum = 171;
		checksum += 85;
		checksum += motorValue;

		Serial2.write(171);
		Serial2.write(85);
		Serial2.write(motorValue);
		Serial2.write(checksum);
	}


	//if the intervaltime has passed
	if (timeDifference > INTERVALTIME){
		//get amount of steps passed in that time
		distanceTraveled = 8 * indexCounterDistance;
		//distanceTraveled is cm 
		//reset the index
		indexCounterDistance = 0;
		//update the previousTime to reset the timer
		previousTime = currentTime;
		
		//make the data to send
		Serial.print("+");
		Serial.print(distanceTraveled);
		Serial.print(",");
		Serial.println(steerPosition);
	}
}
