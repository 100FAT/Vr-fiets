#include <QuadEncoder.h>
// Controller script to control the speed of the vr-bike motor

/*****[Motor PWM]*****/
const int motorPin = 5;
int motorValue;

unsigned long previousTimeMotor = 0;
unsigned long currentTime = 0;
unsigned long timeOutMotor = 30000; //half a minute

/*****[Steering wheel]*****/
const int wheelEncoderAPin = 2;
const int wheelEncoderBPin = 3;
const int wheelEncoderZPin = 4;

QuadEncoder wheelEncoder(1, wheelEncoderAPin, wheelEncoderBPin, 0, wheelEncoderZPin);

const int offsetWheel = -10500; //offset for the wheelPosition value, set this value so the middle of the steering wheel is approximately 0
long wheelPosition = 0;

/*****[Reed sensor]*****/
const int reedPin = 6;
volatile int reedCounter = 0;
int distanceTraveled = 0;
const int distancePerPulse = 50; //cm

unsigned long previousTimeReed = 0;

const unsigned long intervalTimeReed = 20;

void setup(){
	
	//Setting interrupt pins

	pinMode(reedPin, INPUT_PULLUP);
	pinMode(motorPin, OUTPUT);
	attachInterrupt(digitalPinToInterrupt(reedPin), reedInterrupt, FALLING);
	Serial.begin(115200);

	wheelEncoder.setInitConfig();
	wheelEncoder.EncConfig.INDEXTriggerMode = 1;
	wheelEncoder.init();
}

void reedInterrupt(){
	//Record the fact the magnet has passed the reed sensor
	reedCounter ++;
}

void loop(){
	//get a timestamp
	currentTime = millis();

	//if something has been send over Serial
	if(Serial.available() > 0){
		//Throw it away
		Serial.read();
	}

	if(distanceTraveled > 0){
		//if distance is traveled , update the timer
		previousTimeMotor = millis();
		digitalWrite(motorPin, HIGH);
	}

	if((unsigned long)(millis() - previousTimeMotor) >= timeOutMotor){
		//if no movement was for longer then timeout
		digitalWrite(motorPin, LOW);

	}
	wheelPosition = wheelEncoder.read();

	//check if the interval time to send stuff to unity has passed
	if((unsigned long)(currentTime - previousTimeReed) >= intervalTimeReed){
		//get the amount of pulses from the reedsensor and multiply by the distanceperpulse
		distanceTraveled = distancePerPulse * reedCounter;
		//reset the reedCounter for the next time
		reedCounter = 0;
		//update the timer
		previousTimeReed = currentTime;

		//send the data to unity
		Serial.print("+");
		Serial.print(distanceTraveled);
		Serial.print(",");
		Serial.println(wheelPosition + offsetWheel);
	}	
}